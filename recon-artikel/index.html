<h1 id="ein-protokoll-für-den-datenabgleich-im-web-am-beispiel-von-openrefine-und-der-gnd">Ein Protokoll für den Datenabgleich im Web am Beispiel von OpenRefine und der GND</h1>
<p>Fabian Steeg &amp; Adrian Pohl, Hochschulbibliothekszentrum des Landes NRW</p>
<p>Beitrag im Sammelband zur Qualität der Inhaltserschließung, Preprint 2020-12-18.</p>
<p>Fabian Steeg ist Softwareentwickler mit Schwerpunkt Informationssysteme und Open-Source-Software und arbeitet im Bereich Webentwicklung und Datenverarbeitung in der Gruppe Offene Infrastruktur am Hochschulbibliothekszentrum des Landes Nordrhein-Westfalen (hbz). Er ist Mitglied der W3C Entity Reconciliation Community Group und seit 2020 deren Co-Chair. Fabian hat einen Abschluss in Informationsverarbeitung, Allgemeiner Sprachwissenschaft und Geographie von der Universität zu Köln.</p>
<p>Adrian Pohl leitet die Gruppe Offene Infrastruktur des Hochschulbibliothekszentrums des Landes Nordrhein-Westfalen (hbz), die sich mit den verschiedenen Ebenen von Publikation, Austausch und Management bibliothekarischer Daten im Web befasst. Er ist Co-Vorsitzender des Programmkomitees der SWIB-Konferenz (&quot;Semantic Web in Bibliotheken&quot;). Adrian hat Abschlüsse in Kommunikationswissenschaft und Philosophie von der RWTH Aachen sowie in Bibliotheks- und Informationswissenschaft von der TH Köln.</p>
<h1 id="einordnung">Einordnung</h1>
<h2 id="normdaten-und-die-qualität-der-inhaltserschließung">Normdaten und die Qualität der Inhaltserschließung</h2>
<p>Normdaten spielen speziell im Hinblick auf die Qualität der Inhaltserschließung bibliografischer und archivalischer Ressourcen eine wichtige Rolle. Ein konkretes Ziel der Inhaltserschließung ist z. B., dass alle Werke über Hermann Hesse einheitlich zu finden sind. Hier bieten Normdaten eine Lösung, indem z. B. bei der Erschließung einheitlich die GND-Nummer 11855042X für Hermann Hesse verwendet wird. Das Ergebnis ist eine höhere Qualität der Inhaltserschließung vor allem im Sinne von Einheitlichkeit und Eindeutigkeit und, daraus resultierend, eine bessere Auffindbarkeit.</p>
<p>Werden solche Entitäten miteinander verknüpft, z. B. Hermann Hesse mit einem seiner Werke, entsteht ein <em>Knowledge Graph</em>, wie ihn etwa Google bei der Inhaltserschließung des Web verwendet (Singhal 2012). Die Entwicklung des <em>Google Knowledge Graph</em> und das hier vorgestellte Protokoll sind historisch miteinander verbunden: OpenRefine wurde ursprünglich als Google Refine entwickelt, und die Funktionalität zum Abgleich mit externen Datenquellen (Reconciliation<em>)</em> wurde ursprünglich zur Einbindung von Freebase entwickelt, einer der Datenquellen des <em>Google Knowledge Graph</em>. Freebase wurde später in Wikidata integriert. Schon Google Refine wurde zum Abgleich mit Normdaten verwendet, etwa den Library of Congress Subject Headings (Hooland et al. 2013).</p>
<h2 id="reconciliation-als-teil-der-inhaltserschließung">Reconciliation als Teil der Inhaltserschließung</h2>
<p>Bei der Verwendung der GND zur Inhaltserschließung findet bei den Erfassenden ein Abgleich zwischen den vorliegenden Daten (z. B. der Zeichenkette <em>Hermann Hesse</em>) und den Normdaten statt. Über ein System zum Abfragen der GND wird etwa der Eintrag zum Schriftsteller und Nobelpreisträger (11855042X), zum russischen Staatsrat und Arzt (137565259), oder zur Hesse-Biografie von Hugo Ball (4592695-5) zur Verknüpfung ausgewählt. In diesem Sinn ist hier der Begriff des Datenabgleichs bzw. der Reconciliation zu verstehen: als Abgleich von Namen (einer Person, eines Ortes, eines Schlagworts etc.) mit jeweils einem Identifikator innerhalb einer Normdatei.</p>
<p>Der Prozess der Reconciliation selbst kann so als Form oder Teil der Inhaltserschließung gesehen werden. Die Werkzeugunterstützung, Automatisierung und Standardisierung dieses manuellen Schrittes ist Gegenstand dieses Beitrags. Dabei wird dargestellt, wie Prozesse des manuellen Abgleichs (z. B. die Disambiguierung von Hermann Hesse als Schriftsteller) beim automatischen Verfahren formal kodiert werden, etwa durch strukturierte Zusatzinformationen zu Beruf und Lebensdaten.</p>
<p>So erweitert sich der beschriebene Anwendungsbereich über die manuelle Erschließung (gedruckter) bibliografischer und archivalischer Ressourcen in den Bereich der halb- oder vollautomatischen Erschließung mittels Batch-/Stapelverarbeitung, etwa von elektronischen Ressourcen, digitalen Editionen oder sonstigen Forschungsdaten. Ziel des in diesem Beitrag beschriebenen Protokolls ist in diesem Sinn also auch, Normdaten als zentrales Element der Inhaltserschließung für neue Anwendungsfälle zugänglich zu machen.</p>
<h2 id="terminologie-und-abgrenzung">Terminologie und Abgrenzung</h2>
<p>Die beschriebenen Konzepte sollen im Folgenden kurz von verwandten Begriffen und Verfahren abgegrenzt werden. Es handelt sich bei der Reconciliation im engeren Sinn wie oben beschrieben um das Identifizieren eindeutiger Entitäten in einer Wissensbasis (Knowledge Base) durch die Ermittlung von Identifikatoren für vorliegende Eigennamen. Die identifizierten Entitäten selbst haben Attribute, die sie zum Teil wiederum mit weiteren Entitäten verbinden (wenn diese Attribute als Werte Identifikatoren enthalten, in der GND z. B. die Verbindung von Hermann Hesse mit seinem Geburtsort <em>Calw</em>). In diesem Sinn handelt es sich bei der Reconciliation um die Verortung, und damit Disambiguierung, von Daten in einem Knowledge Graph.</p>
<p>Im Kontext von maschineller Sprachverarbeitung und Information Retrieval spricht man innerhalb des weiten Feldes der Informationsextraktion von der Eigennamenerkennung (Named Entity Recognition). Für den über die bloße Erkennung eines Eigennamens (z. B. <em>Hermann Hesse</em> ist ein Name) hinausgehenden Fall der Verknüpfung mit Normdaten (z. B. <em>Hermann Hesse</em> ist 11855042X) hat sich der Begriff <em>(Named) Entity Linking</em> etabliert. Durch die eindeutige Verknüpfung mit einer Entität handelt es sich bei dieser zugleich um eine Form von Wortsinndisambiguierung (Word Sense Disambiguation), hier z. B. von <em>Hermann Hesse</em> als Schriftsteller und Nobelpreisträger (11855042X) gegenüber anderen Bedeutungen wie dem russischen Staatsrat und Arzt (137565259) oder dem Werk von Hugo Ball (4592695-5). Daher wird hier auch von <em>Named Entity Disambiguation</em> (Slawski 2015) oder <em>Named Entity Normalization</em> (Khalid, Jijkoun, and Rijke 2008) gesprochen. Hier schließt sich der Kreis zur bibliothekarischen Terminologie: Das im Folgenden beschriebene Protokoll dient zur Normalisierung von Entitäten mittels Normdaten.</p>
<h2 id="protokolle-und-standards">Protokolle und Standards</h2>
<p>Sowohl in klassischen Bibliothekssystemen als auch im Web spielen standardisierte Datenformate und Protokolle eine zentrale Rolle. Formate wie MAB oder MARC und Protokolle wie Z39.50 ermöglichen einen institutions- und systemübergreifenden Datenaustausch und damit die Nachnutzung und Zusammenführung, etwa in Verbundkatalogen. Formate wie HTML oder JSON und Protokolle wie HTTP ermöglichen den weltweiten Datenaustausch im Web. So ist es erstrebenswert, den hier beschriebenen Datenabgleich über ein standardisiertes Protokoll durchzuführen bzw. ein solches zu entwickeln (Delpeuch et al. 2020), um institutionsübergreifend einheitlich auf zentrale Normdaten zuzugreifen. In diesem Sinn werden im nächsten Abschnitt die Details des Protokolls für den Datenabgleich im Web am Beispiel von OpenRefine und der GND dargestellt. Der verwendete Reconciliation-Dienst von lobid-gnd (Steeg, Pohl, and Christoph 2019) basiert auf den als RDF publizierten GND-Daten der Deutschen Nationalbibliothek (DNB) (Hauser 2014).</p>
<h1 id="protokoll">Protokoll</h1>
<p>Der folgende Abschnitt beschreibt die einzelnen Elemente des Protokolls für den Datenabgleich im Web und ihre Verwendung am Beispiel von OpenRefine und der GND. Das Protokoll hat seinen Ursprung in der Implementierung der Netzwerkkommunikation in OpenRefine. Ausgehend davon soll es im Rahmen des W3C standardisiert werden (Delpeuch et al. 2020). Dies hat gegenüber dem umgekehrten Ansatz (zuerst wird ein Protokoll standardisiert, dann wird es implementiert) den Vorteil, dass das Protokoll erwiesenermaßen praxistauglich ist. Es entspricht in diesem Aspekt auch der in der Internet Engineering Task Force (IETF) entstandenen pragmatischen Grundhaltung der Internet-Standardisierung (Alvestrand and Lie 2009). Die folgende Beschreibung des Protokolls kann anhand der Implementierung in lobid-gnd (Steeg, Pohl, and Christoph 2019) praktisch nachvollzogen werden.<a href="#fn1" class="footnoteRef" id="fnref1"><sup>1</sup></a></p>
<h2 id="json">JSON</h2>
<p>Das im Folgenden beschriebene Protokoll basiert auf JSON, dem bereits ab 2005 sehr populären und spätestens nach seiner Standardisierung 2013 weit etablierten Format für Web-basierten Datenaustausch (Target 2017). Grundelement von JSON ist eine Attribut-Wert Zuordnung, z. B.:</p>
<pre><code>{
  &quot;Attribut_1&quot;: &quot;Wert_1&quot;,
  &quot;Attribut_2&quot;: &quot;Wert_2&quot;
}</code></pre>
<p>Die Metadaten, Anfragen und Antworten dieses Protokolls werden mit JSON formuliert.</p>
<h2 id="service">Service</h2>
<p>Ein <em>reconciliation service</em><a href="#fn2" class="footnoteRef" id="fnref2"><sup>2</sup></a> beschreibt sich selbst in einem <em>service manifest</em>. Dieses JSON-Dokument definiert mindestens einen Namen in name, ein Präfix zur Identifikation der gelieferten Entitäten (z. B. zur Identifikation einer GND-Nummer wie 118624822 als https://d-nb.info/gnd/118624822) in identifierSpace und den Typ der gelieferten Entitäten (und damit die Ontologie samt verfügbaren Properties für die Entitäten) in schemaSpace:</p>
<pre><code>{
  &quot;name&quot;: &quot;GND reconciliation for OpenRefine&quot;,
  &quot;identifierSpace&quot;: &quot;https://d-nb.info/gnd/&quot;,
  &quot;schemaSpace&quot;: &quot;https://d-nb.info/standards/elementset/gnd#AuthorityResource&quot;
}</code></pre>
<h2 id="reconciliation-queries">Reconciliation-Queries</h2>
<p>Ein Dienst mit einem solchen <em>service manifest</em> kann in einem <em>reconciliation client</em> eingebunden werden und steht dann für <em>reconciliation queries</em> zur Verfügung (s. Abb. 1). Das <em>service manifest</em> kann weitere, optionale Hilfsdienste und Unterstützung für <em>data extension</em> deklarieren (s. unten).</p>
<p>Hier Abb1_Steeg.png einfügen</p>
<p>Abb. 1: Der Reconciliation-Dialog mit zahlreichen Konfigurationsmöglichkeiten in OpenRefine</p>
<h3 id="einfache-anfragen">Einfache Anfragen</h3>
<p>In der einfachsten Form werden nur Namen an den Dienst geschickt. Dies erfolgt für alle abzugleichenden Werte in einer einzigen Anfrage. Auf Ebene der OpenRefine-Oberfläche bedeutet dies die Auswahl der entsprechenden Spalte (z. B. <em>Name</em> in Abb. 1).</p>
<p>Auf Ebene des Protokolls handelt es sich um ein JSON-Objekt, bei dem jeder Wert durch einen eindeutigen Schlüssel (hier q1 und q2) identifiziert wird. Der Wert eines Attributs muss selbst keine Zeichenkette sein (wie &quot;Wert_1&quot; im ersten JSON-Beispiel oben), sondern kann selbst wieder JSON sein, z. B. { &quot;query&quot;: &quot;Hans-Eberhard Urbaniak&quot; }. So kann mit JSON eine geschachtelte Struktur ausgedrückt werden.</p>
<p>Für eine Spalte bzw. Anfrage mit zwei Werten stellt sich eine minimale Anfrage dann so dar:</p>
<pre><code>{
  &quot;q1&quot;: { &quot;query&quot;: &quot;Hans-Eberhard Urbaniak&quot; },
  &quot;q2&quot;: { &quot;query&quot;: &quot;Ernst Schwanhold&quot; }
}</code></pre>
<p>Eine solche minimale <em>reconciliation query</em> lässt sich etwa mit folgender URL im Browser durchführen:</p>
<p>https://lobid.org/gnd/reconcile/?queries={&quot;q1&quot;:{&quot;query&quot;:&quot;Twain, Mark&quot;}}</p>
<p>Die im Browser ausgelieferte Antwort ist ein JSON-Dokument. Zur komfortableren Anzeige im Browser, etwa mit Syntax-Coloring und einklappbaren Unterabschnitten, existieren diverse JSON-Browser-Plugins. Auf der Kommandozeile können die Daten etwa mit dem vielseitigen Werkzeug jq verarbeitet werden:<a href="#fn3" class="footnoteRef" id="fnref3"><sup>3</sup></a></p>
<p>curl --data 'queries={&quot;q1&quot;:{&quot;query&quot;:&quot;Twain, Mark&quot;}}' https://lobid.org/gnd/reconcile/ | jq</p>
<p>Die Antwort besteht aus einer Reihe von Vorschlägen (in JSON als Array innerhalb von [ und ] ausgedrückt) für jedes Element der Anfrage (hier gekürzt: nur q1 und die ersten zwei Vorschläge):</p>
<pre><code>{
  &quot;q1&quot;: {
    &quot;result&quot;: [
      {
        &quot;id&quot;: &quot;118624822&quot;,
        &quot;name&quot;: &quot;Twain, Mark&quot;,
        &quot;score&quot;: 84.15378,
        &quot;match&quot;: true,
        &quot;type&quot;: [{&quot;id&quot;: &quot;DifferentiatedPerson&quot;, &quot;name&quot;: &quot;Individualisierte Person&quot;}]
      },
      {
        &quot;id&quot;: &quot;1045623490&quot;,
        &quot;name&quot;: &quot;Bezirkszentralbibliothek Mark Twain. Schreibwerkstatt&quot;,
        &quot;score&quot;: 78.29902,
        &quot;match&quot;: false,
        &quot;type&quot;: [{&quot;id&quot;: &quot;CorporateBody&quot;, &quot;name&quot;: &quot;Körperschaft&quot;}]
      }
    ]
  }
}</code></pre>
<p>Als erster Vorschlag erscheint hier also Mark Twain selbst (118624822, Typ Individualisierte Person), als zweiter Vorschlag eine Körperschaft mit dem Identifikator 1045623490. Die Eindeutigkeit der Identifikatoren ergibt sich durch den im <em>service manifest</em> angegebenen identifierSpace (s. Abschnitt <em>Service</em>). Der gemeinsame Namensraum https://d-nb.info/gnd/ für GND-Nummern ermöglicht so die Interoperabilität verschiedener Dienste auf Basis der GND.</p>
<p>Neben Identifikator und Typ enthalten die Vorschläge die Felder name, score und match als Details zum jeweiligen Vorschlag. Score ist ein Maß des Dienstes für die Übereinstimmung des Vorschlages mit der Anfrage (d. h. je höher der score, desto besser der Vorschlag) und match drückt per Wahrheitswert aus, ob der Vorschlag nach internen Kriterien des Dienstes als Treffer zu dem Vorschlag bewertet wird.</p>
<p>Diese Vorschläge können den Nutzenden im <em>reconciliation client</em> angezeigt werden (s. Abb. 2, pro Name sehen wir die entsprechenden Vorschläge, bzw. den Namen in Fett bei der automatisch als Treffer gewerteten Entität).</p>
<h3 id="weitere-metadaten">Weitere Metadaten</h3>
<p>Dadurch, dass mit jedem Element der Anfrage (z. B. q1 oben) wieder JSON assoziiert ist, können jedem Element der Anfrage zusätzlich zum Namen weitere Metadaten hinzugefügt werden, etwa der gesuchte Entitätstyp in type oder eine Begrenzung der vom Dienst gelieferten Vorschläge in limit:</p>
<pre><code>{
  &quot;q0&quot;: {
    &quot;query&quot;: &quot;Christel Hanewinckel&quot;,
    &quot;type&quot;: &quot;DifferentiatedPerson&quot;,
    &quot;limit&quot;: 5
  },
  &quot;q1&quot;: {
    &quot;query&quot;: &quot;Franz Thönnes&quot;,
    &quot;type&quot;: &quot;DifferentiatedPerson&quot;,
    &quot;limit&quot;: 5
  }
}</code></pre>
<p>Diese können in einem <em>reconciliation client</em> bei der Nutzung konfiguriert werden (s. Abb. 1, Typauswahl oben links: <em>Reconcile each cell to an entity of one of these types</em>, Beschränkung unten links: <em>Maximum number of candidates to return</em>).</p>
<p>Für eine höhere Transparenz der oben beschriebenen Bewertung in der Antwort (score, match) kann ein <em>reconciliation service</em> für jedes result auch spezifische features zurückgegeben, die eine differenziertere Bewertung der Vorschläge durch den <em>reconciliation client</em> ermöglichen, z. B. eine separate Bewertung der Übereinstimmung des Namens (in name_tfidf) bzw. des angeforderten Typs (in type_match):</p>
<pre><code>&quot;features&quot;: [
  {
    &quot;id&quot;: &quot;name_tfidf&quot;,
    &quot;value&quot;: 334.188
  },
  {
    &quot;id&quot;: &quot;type_match&quot;,
    &quot;value&quot;: 13.78
  }
]</code></pre>
<p>Auf dieser Basis könnte ein <em>reconciliation client</em> neben der Auswertung des match-Wertes (s. Abb. 1, links unten: <em>Auto-match candidates with high confidence</em>) selbst entscheiden, ob etwa eine Übereinstimmung beim angeforderten Typ (d.h. ein hoher Wert für type_match) eine geringere Übereinstimmung des Namens (d.h. einen niedrigen Wert für name_tfidf) ausgleicht und doch als Treffer zu werten ist.</p>
<h3 id="zusätzliche-daten">Zusätzliche Daten</h3>
<p>Neben dem abzugleichenden Namen und den oben beschriebenen Metadaten können weitere Daten mitgeschickt werden, um die Qualität des Abgleichs zu erhöhen, d. h. um mit höherer Wahrscheinlichkeit den korrekten Identifikator vom Dienst angeboten zu bekommen. Dies können bei Personen etwa Lebensdaten oder Berufe sein. Auf Ebene der OpenRefine-Oberfläche sind diese weiteren Daten zusätzliche Spalten der Tabelle (z. B. Spalten Beruf, Geburtsjahr, Sterbejahr; s. Abb. 1, oben rechts: <em>Also use relevant details from other columns</em>). Auf Ebene des Protokolls werden diese als properties abgebildet:<a href="#fn4" class="footnoteRef" id="fnref4"><sup>4</sup></a></p>
<pre><code>&quot;properties&quot;: [
  {
    &quot;pid&quot;: &quot;professionOrOccupation&quot;,
    &quot;v&quot;: &quot;Politik*&quot;
  },
  {
    &quot;pid&quot;: &quot;affiliation&quot;,
    &quot;v&quot;: &quot;http://d-nb.info/gnd/2022139-3&quot;
  }
]</code></pre>
<p>Hier Abb2_Steeg.png einfügen</p>
<p>Abb. 2: Ergebnisse der Reconciliation mit Vorschlägen und Vorschau zur Auswahl von Kandidaten</p>
<h2 id="hilfsdienste">Hilfsdienste</h2>
<p>Neben der zentralen Funktionalität der <em>reconciliation queries</em> kann ein <em>reconciliation service</em> weitere Dienste anbieten. Dies sind zum einen Hilfsdienste, die die Kernfunktionalität erweitern, insbesondere in Form von Vorschauen und Vorschlägen, sowie zum anderen Dienste zur Verwendung der abgeglichenen Entitäten zur Datenanreicherung der lokalen Datensätze (<em>data extension</em>).</p>
<h3 id="anzeige">Anzeige</h3>
<p>Im Wesentlichen gibt es zwei Dienste zur Anzeige von Entitäten. Zum einen kann der <em>reconciliation service</em> in seinem <em>service manifest</em> deklarieren, wo Entitäten auf Basis eines Identifikators angezeigt werden können. Dies erfolgt über einen Eintrag view mit einer URL, die einen Platzhalter für den Identifikator enthält, z. B.:</p>
<pre><code>&quot;view&quot;: {
  &quot;url&quot;: &quot;https://lobid.org/gnd/{{id}}&quot;
}</code></pre>
<p>Ein <em>reconciliation client</em> kann damit einen Link zu einer Entität erzeugen, indem in &quot;https://lobid.org/gnd/{{id}}&quot; die Zeichenkette {{id}} durch den eigentlichen Identifikator ersetzt wird, um z. B. oben den ersten Vorschlag zu Mark Twain mit einem Link zu https://lobid.org/gnd/118624822 zu hinterlegen (s. Abb. 2, Vorschläge sind mit Links hinterlegt).</p>
<p>Für eine engere Integration in einen <em>reconciliation client</em> gibt es darüber hinaus die Möglichkeit, eine Vorschau zu liefern. Der <em>reconciliation service</em> definiert dazu in vergleichbarer Form einen Service in seinem <em>service manifest</em>:</p>
<pre><code>&quot;preview&quot;: {
  &quot;height&quot;: 100,
  &quot;width&quot;: 320,
  &quot;url&quot;: &quot;https://lobid.org/gnd/{{id}}.preview&quot;
}</code></pre>
<p>Im Unterschied zum view wird hier eine Größe definiert, so dass der Client einen entsprechend großen Vorschaubereich erzeugen kann. Hier liefert der Dienst wie bei view HTML, das direkt angezeigt werden kann, allerdings muss dafür bei preview keine neue Seite verlinkt werden, sondern die Vorschau kann etwa in einem Popup angezeigt werden (s. Abb. 2, Vorschau für Kandidaten mit Bild, Lebensdaten, Beruf und Typ).</p>
<h3 id="vorschläge">Vorschläge</h3>
<p>Die suggest<em>-</em>Hilfsdienste dienen zur Anzeige von Vorschlägen an verschiedenen Stellen des Datenabgleichs in einem <em>reconciliation client</em>. Vorgeschlagen werden können Entitäten, Properties und Typen. Dazu wird jeweils im <em>service manifest</em> der eigentliche Vorschlagsdienst, sowie analog zum preview oben, ein so genannter Flyout-Dienst für kleine, integrierte Darstellungen deklariert:</p>
<pre><code>&quot;suggest&quot;: {
    &quot;property&quot;: {
      &quot;service_url&quot;: &quot;https://lobid.org/gnd/reconcile&quot;,
      &quot;service_path&quot;: &quot;/suggest/property&quot;,
      &quot;flyout_service_path&quot;: &quot;/flyout/property?id=${id}&quot;
    },
    &quot;entity&quot;: {
      &quot;service_url&quot;: &quot;https://lobid.org/gnd/reconcile&quot;,
      &quot;service_path&quot;: &quot;/suggest/entity&quot;,
      &quot;flyout_service_path&quot;: &quot;/flyout/entity?id=${id}&quot;
    },
    &quot;type&quot;: {
      &quot;service_url&quot;: &quot;https://lobid.org/gnd/reconcile&quot;,
      &quot;service_path&quot;: &quot;/suggest/type&quot;,
      &quot;flyout_service_path&quot;: &quot;/flyout/type?id=${id}&quot;
    }
}</code></pre>
<p>Alle Vorschlagsdienste erwarten einen Query-Parameter prefix, in dem die bisher von den Nutzenden eingegebene Zeichenkette übergeben wird. Dies dient im Client etwa dazu, vorzuschlagen, mit welcher GND-Property mitgeschickte Daten assoziiert werden (s. oben, Zusätzliche Daten). Wird im Client etwa an der entsprechenden Stelle beruf eingegeben, wird intern folgende Anfrage an den property<em>-</em>Hilfsdienst gesendet:</p>
<p>https://lobid.org/gnd/reconcile/suggest/property?prefix=beruf</p>
<p>Die Antwort zu dieser Anfrage lautet (kann wie oben im Browser oder über das Kommandozeilenwerkzeug curl nachvollzogen werden):</p>
<pre><code>{
  &quot;code&quot;: &quot;/api/status/ok&quot;,
  &quot;status&quot;: &quot;200 OK&quot;,
  &quot;prefix&quot;: &quot;beruf&quot;,
  &quot;result&quot;: [
    {
      &quot;id&quot;: &quot;professionOrOccupation&quot;,
      &quot;name&quot;: &quot;Beruf oder Beschäftigung&quot;
    },
    {
      &quot;id&quot;: &quot;professionOrOccupationAsLiteral&quot;,
      &quot;name&quot;: &quot;Beruf oder Beschäftigung (Literal)&quot;
    },
    {
      &quot;id&quot;: &quot;professionalRelationship&quot;,
      &quot;name&quot;: &quot;Berufliche Beziehung&quot;
    }
  ]
}</code></pre>
<p>Die drei gelieferten properties können dann den Nutzenden zur Auswahl vorgeschlagen werden (s. Abb. 1, rechts oben: <em>Also use relevant details from other columns</em> sowie Abb. 3, links: <em>Add Property</em>). Analog kann vor dem Abgleich ein spezifischer Typ vorgeschlagen werden (type-Suggest-Dienst, s. Abb. 1, unten links: <em>Reconcile against type</em>), oder nach dem Abgleich gezielt nach einem Treffer gesucht werden (<em>entity suggest service</em>, s. Abb. 2, unterhalb der Vorschläge: <em>Search for match</em>).</p>
<h2 id="data-extension">Data Extension</h2>
<p>Das Protokoll zur <em>data extension</em> ermöglicht eine Datenanreicherung auf Basis der abgeglichenen Treffer. Es besteht aus zwei wesentlichen Teilen: erstens der Kommunikation über die zur Datenanreicherung verfügbaren <em>properties</em> (s. Abb. 3, linker Bereich) und zweitens der eigentlichen Anreicherung mit den Werten der ausgewählten <em>properties</em> (s. Abb. 3, rechter Bereich).</p>
<p>Hier Abb3_Steeg.png einfügen</p>
<p>Abb. 3: Dialog zur <em>data extension</em> mit verfügbaren <em>properties</em> und Vorschau für ergänzte Spalten</p>
<h3 id="property-vorschläge">Property-Vorschläge</h3>
<p>Zunächst muss wieder das <em>service manifest</em> die Unterstützung für Property-Vorschläge deklarieren:</p>
<pre><code>&quot;propose_properties&quot;: {
  &quot;service_url&quot;: &quot;https://lobid.org&quot;,
  &quot;service_path&quot;: &quot;/gnd/reconcile/properties&quot;
}</code></pre>
<p>Ein solcher Dienst liefert die für einen bestimmten Typ (z. B. Work) verfügbaren <em>properties</em>:</p>
<p>GET https://lobid.org/gnd/reconcile/properties?type=Work</p>
<p>Hier eine gekürzte Antwort:</p>
<pre><code>{
  &quot;type&quot;: &quot;Work&quot;,
  &quot;properties&quot;: [
    {
      &quot;id&quot;: &quot;abbreviatedNameForTheWork&quot;,
      &quot;name&quot;: &quot;Abgekürzter Name des Werks&quot;
    },
    {
      &quot;id&quot;: &quot;firstAuthor&quot;,
      &quot;name&quot;: &quot;Erste Verfasserschaft&quot;
    },
    {
      &quot;id&quot;: &quot;preferredName&quot;,
      &quot;name&quot;: &quot;Bevorzugter Name&quot;
    },
    {
      &quot;id&quot;: &quot;relatedConferenceOrEvent&quot;,
      &quot;name&quot;: &quot;In Beziehung stehende Konferenz oder Veranstaltung&quot;
    }
  ]
}</code></pre>
<p>Aus diesen <em>properties</em> können dann von Nutzenden diejenigen ausgewählt werden, die zur eigentlichen Datenanreicherung verwendet werden sollen (s. Abb. 3, links: <em>Suggested Properties</em>).</p>
<h3 id="extension-anfragen">Extension-Anfragen</h3>
<p>In der einfachsten Form werden bei der eigentlichen Anfrage zur <em>data extension</em> die Identifikatoren der zu verwendenden Entitäten sowie die gewünschten <em>properties</em> geschickt, z. B.:</p>
<pre><code>{
    &quot;ids&quot;: [
        &quot;1081942517&quot;,
        &quot;4791358-7&quot;
    ],
    &quot;properties&quot;: [
        {&quot;id&quot;: &quot;preferredName&quot;},
        {&quot;id&quot;: &quot;firstAuthor&quot;}
    ]
}</code></pre>
<p>Als vollständige Anfrage etwa:</p>
<pre><code>GET https://lobid.org/gnd/reconcile/?extend=
{&quot;ids&quot;:[&quot;1081942517&quot;,&quot;4791358-7&quot;],&quot;properties&quot;:[{&quot;id&quot;:&quot;preferredName&quot;},{&quot;id&quot;:&quot;firstAuthor&quot;}]}</code></pre>
<p>Die Antwort liefert die entsprechenden Daten:</p>
<pre><code>{
  &quot;meta&quot;: [
    {&quot;id&quot;: &quot;preferredName&quot;, &quot;name&quot;: &quot;Bevorzugter Name&quot;},
    {&quot;id&quot;: &quot;firstAuthor&quot;, &quot;name&quot;: &quot;Erste Verfasserschaft&quot;}
  ],
  &quot;rows&quot;: {
    &quot;1081942517&quot;: {
      &quot;preferredName&quot;: [{&quot;str&quot;: &quot;Autobiography of Mark Twain&quot;}],
      &quot;firstAuthor&quot;: [{&quot;id&quot;: &quot;118624822&quot;, &quot;name&quot;: &quot;Twain, Mark&quot;}]
    },
    &quot;4791358-7&quot;: {
      &quot;preferredName&quot;: [{&quot;str&quot;: &quot;Die größere Hoffnung (1960)&quot;}],
      &quot;firstAuthor&quot;: [{&quot;id&quot;: &quot;118501232&quot;,&quot;name&quot;: &quot;Aichinger, Ilse&quot;}]
    }
  }
}</code></pre>
<p>Es erscheinen zunächst, unter meta, Informationen zu den angereicherten Daten: die Identifikatoren und Namen für die angereicherten Properties. Dies ist vergleichbar mit der Header-Zeile einer Tabelle (s. Abb. 3, rechts, fett gedruckte Kopfzeile). Im Folgenden erscheinen die einzelnen Datensätze in rows, jeweils unter dem Identifikator der Entität (hier das Werk), z. B. 1081942517, die jeweiligen Properties (hier preferredName und firstAuthor). Die Struktur der Werte unterscheidet sich, da die Ansetzungsformen aus preferredName einfache Zeichenketten sind, während in firstAuthor GND-Entitäten enthalten sind, die jeweils wieder einen Identifikator und einen Namen haben. Diese Daten können dann im <em>reconciliation client</em> den lokalen Daten hinzugefügt werden (s. Abb. 3, rechts).</p>
<p>Neben dieser Definition von Properties allein über ihre Identifikatoren (z. B. oben {&quot;id&quot;:&quot;preferredName&quot;}) besteht die Möglichkeit, Properties zu konfigurieren. So kann etwa die Zahl der Werte für ein bestimmtes Feld bei der Anreicherung eingeschränkt werden, wenn z. B. nicht alle, sondern nur die ersten fünf Namensvarianten zurückgegeben werden sollen. Die Unterstützung für eine solche Konfiguration deklariert der Dienst zunächst etwa so:</p>
<pre><code>&quot;property_settings&quot;: [
  {
    &quot;name&quot;: &quot;limit&quot;,
    &quot;label&quot;: &quot;Limit&quot;,
    &quot;type&quot;: &quot;number&quot;,
    &quot;default&quot;: 0,
    &quot;help_text&quot;: &quot;Maximum number of values to return per row (0 for no limit)&quot;
  }
]</code></pre>
<p>Neben der freien Eingabe soll die Konfiguration oft auch mit festen Werten erfolgen. So können für bestimmte Felder in der GND etwa Identifikatoren oder Namen geliefert werden. Damit Nutzende dies je nach Bedarf im <em>reconciliation client</em> auswählen können, definiert der Dienst zusätzlich zu den Werten oben choices, z. B.:</p>
<pre><code>&quot;property_settings&quot;: [
  {
    &quot;name&quot;: &quot;content&quot;,
    &quot;label&quot;: &quot;Content&quot;,
    &quot;type&quot;: &quot;select&quot;,
    &quot;default&quot;: &quot;literal&quot;,
    &quot;help_text&quot;: &quot;Content type: ID or literal&quot;,
    &quot;choices&quot;: [
      { &quot;value&quot;: &quot;id&quot;, &quot;name&quot;: &quot;ID&quot; },
      { &quot;value&quot;: &quot;literal&quot;, &quot;name&quot;: &quot;Literal&quot; }
    ]
  }
]</code></pre>
<p>Die in choices beschriebenen Optionen können dann in einem <em>reconciliation client</em> bei der Auswahl und Konfiguration der zur Datenanreicherung zu verwendenden Properties angezeigt werden, z. B. in Form eines Auswahlmenüs für die oben im <em>service manifest</em> deklarierten Werte (s. Abb. 3, rechts, jeweils: <em>configure</em> öffnet einen Konfigurationsdialog).</p>
<p>Bei der entsprechenden <em>data extension</em> Anfrage wird die jeweilige Konfiguration dann mitgeschickt, z. B.:</p>
<pre><code>{
  &quot;ids&quot;: [
    &quot;10662041X&quot;,
    &quot;1064905412&quot;
  ],
  &quot;properties&quot;: [
    {
      &quot;id&quot;: &quot;variantName&quot;,
      &quot;settings&quot;: {
        &quot;limit&quot;: &quot;5&quot;
      }
    },
    {
      &quot;id&quot;: &quot;professionOrOccupation&quot;
    },
    {
      &quot;id&quot;: &quot;geographicAreaCode&quot;,
      &quot;settings&quot;: {
        &quot;limit&quot;: &quot;1&quot;,
        &quot;content&quot;: &quot;id&quot;
      }
    }
  ]
}</code></pre>
<p>Hier werden für zwei Entitäten jeweils drei Properties angefordert: erstens variantName (konfiguriert mit einem limit von 5), zweitens professionOrOccupation (ohne Konfiguration) und schließlich drittens geographicAreaCode mit einem limit von 1 und als content den Identifikator (über die im <em>service manifest</em> deklarierte Option mit value: id). Hier werden die Daten also mit maximal fünf Namensvarianten, allen verfügbaren Berufen und einem Ländercode angereichert.</p>
<h1 id="ausblick">Ausblick</h1>
<p>Auf Basis dieser Darstellung des Protokolls und seiner Verwendung in <em>OpenRefine</em> sollen die folgenden zwei Abschnitte einen Ausblick auf die Arbeiten der W3C Entity Reconciliation Community Group und das weitergehende Ökosystem rund um die Reconciliation-API geben.</p>
<h2 id="w3c-community-group">W3C Community Group</h2>
<p>Aufgabe der <em>W3C Entity Reconciliation Community Group</em> ist die Entwicklung einer Web-API, mit der Datenanbieter einen Abgleich von Drittanbieterdaten mit den eigenen Identifikatoren ermöglichen können. Ausgangspunkt der Community Group bildet die beschriebene Implementierung in OpenRefine. Diese API soll zunächst dokumentiert und dann auf Basis ihrer Nutzung (s. Delpeuch 2019) zu einem Standard weiterentwickelt werden.<a href="#fn5" class="footnoteRef" id="fnref5"><sup>5</sup></a> Als ein Beispiel sei hier eine Diskussion zur Frage der Nachvollziehbarkeit von Algorithmen zur Bewertung (<em>Scoring</em>) von Reconciliation-Kandidaten genannt<a href="#fn6" class="footnoteRef" id="fnref6"><sup>6</sup></a>, die anschließend zu einer Erweiterung des Protokolls geführt hat<a href="#fn7" class="footnoteRef" id="fnref7"><sup>7</sup></a>.</p>
<p>Diese Entwicklung einer Web-API durch die Community Group umfasst die Arbeit an den eigentlichen Spezifikationen, der Satzung der Gruppe, einer Webanwendung zum Testen von Reconciliation-Diensten sowie der Erfassung des Reconciliation-Ökosystems aus Diensten, Clients und sonstiger Software rund um die Reconciliation-API. Die konkrete Arbeit findet innerhalb einer GitHub-Organisation<a href="#fn8" class="footnoteRef" id="fnref8"><sup>8</sup></a> statt. Der Aufgabenbereich der Gruppe schließt neben den etablierten Anwendungsfällen des Zusammenführens von Daten aus verschiedenen Quellen auch ähnliche Anwendungsfälle wie die Deduplizierung von Daten aus einer einzigen Quelle ein. Die Gruppe steht allen Interessierten offen und freut sich über jegliche Art von Mitarbeit und Unterstützung.</p>
<h2 id="reconciliation-ökosystem">Reconciliation-Ökosystem</h2>
<p>Die <em>Reconciliation service test bench</em>, eine Webanwendung zum Testen von Reconciliation-Diensten, ist zugleich ein Werkzeug für die Entwicklung eines eigenen Reconciliation-Dienstes und in Form einer zentralen Instanz<a href="#fn9" class="footnoteRef" id="fnref9"><sup>9</sup></a> eine Übersicht der in Wikidata verzeichneten<a href="#fn10" class="footnoteRef" id="fnref10"><sup>10</sup></a> Reconciliation-Dienste mit ihren jeweils unterstützen Features.</p>
<p>Über diese Übersicht der Dienste hinaus, gibt es im Rahmen der Community Group eine Erfassung des Reconciliation-Ökosystems von Diensten, Clients und sonstiger Software rund um die Reconciliation-API<a href="#fn11" class="footnoteRef" id="fnref11"><sup>11</sup></a>. Hier findet sich etwa eine Übersicht alternativer Clients, die statt OpenRefine mit einem Reconciliation-Dienst kommunizieren, z. B. das Mapping-Tool Cocoda (Balakrishnan 2016) oder die Alma-Refine-App<a href="#fn12" class="footnoteRef" id="fnref12"><sup>12</sup></a> mit GND-Integration<a href="#fn13" class="footnoteRef" id="fnref13"><sup>13</sup></a>.</p>
<p>Im Reconciliation-Ökosystem finden sich also reichlich Dokumentation, Werkzeuge und Beispiele für die Entwicklung eigener Reconciliation-Dienste. Über einen solchen Dienst kann jeder Datenanbieter, der eigene Identifikatoren prägt, seine Daten über eine einheitliche API zur Integration durch Dritte zur Verfügung stellen. So wird, auch über den Bereich der Inhaltserschließung hinaus, durch das beschriebene Protokoll eine einheitliche Erfassung von Entitäten ermöglicht, ohne dass diese in allen vorliegenden Datenquellen einheitlich identifiziert sein müssen.</p>
<h1 id="literaturverzeichnis">Literaturverzeichnis</h1>
<p>Alvestrand, Harald Tveit u. Håkon Wium Lie: Development of Core Internet Standards: The Work of IETF and W3C. In: Internet Governance - Infrastructure and Institutions. Hrsg. von Lee A. Bygrave u. Jon Bing. S. 126–146. Oxford University Press 2009. <a href=""><em>http://dblp.uni-trier.de/db/books/collections/BB2009.html#AlvestrandL09</em></a> (4.12.2020).</p>
<p>Balakrishnan, Uma: DFG-Projekt Coli-Conc: Das Mapping Tool ‘Cocoda’. O-Bib. Das Offene Bibliotheksjournal / Herausgegeben Vom VDB. 3 (1). S. 11–16. 2016. doi:<a href="https://doi.org/10.5282/o-bib/2016H1S11-16"><em>https://doi.org/10.5282/o-bib/2016H1S11-16</em></a>.</p>
<p>Delpeuch, Antonin: <span id="pubtitle" class="anchor"></span>A survey of OpenRefine reconciliation services. 2019. arXiv:1906.08092:<a href="http://arxiv.org/abs/1906.08092"><em>http://arxiv.org/abs/1906.08092</em></a>.</p>
<p>Delpeuch, Antonin, Adrian Pohl, Fabian Steeg, Thad Guidry Sr. u. Osma Suominen: Draft: Reconciliation Service API – a Protocol for Data Matching on the Web. <a href="https://reconciliation-api.github.io/specs/latest/"><em>https://reconciliation-</em></a><a href="https://reconciliation-api.github.io/specs/latest/"><em>api.github.io/specs/latest/</em></a> (4.12.2020).</p>
<p>Hauser, Julia: Der Linked Data Service der Deutschen Nationalbibliothek. Dialog Mit Bibliotheken 2014/1 - 34. <a href="https://d-nb.info/1058935003/34"><em>https://d-</em></a><a href="https://d-nb.info/1058935003/34"><em>nb.info/1058935003/34</em></a> (4.12.2020).</p>
<p>Hooland, Seth van, Ruben Verborgh, Max De Wilde, Johannes Hercher, Erik Mannens u. Rik Van de Walle: Evaluating the Success of Vocabulary Reconciliation for Cultural Heritage Collections. Journal of the Association for Information Science and Technology 64 (3). S. 464–479. 2013. doi:<a href="https://doi.org/10.5282/o-bib/2016H1S11-16"><em>https://doi.org/</em></a><a href="https://doi.org/10.1002/asi.22763"><em>10.1002/asi.22763</em></a>.</p>
<p>Khalid, Mahboob A., Valentin Jijkoun u. Maarten de Rijke: The Impact of Named Entity Normalization on Information Retrieval for Question Answering. In: 30th European Conference on Information Retrieval (ECIR 2008). S. 705–710. LNCS 4956. Springer 2008. <a href="https://core.ac.uk/display/238935353"><em>https://core.ac.uk/display/238935353</em></a> (4.12.2020).</p>
<p>Singhal, Amit: Introducing the Knowledge Graph: Things, Not Strings. 2012. <a href="https://www.blog.google/products/search/introducing-knowledge-graph-things-not/"><em>https://www.blog.google/products/search/introducing-</em></a><a href="https://www.blog.google/products/search/introducing-knowledge-graph-things-not/"><em>knowledge-graph-things-not/</em></a> (4.12.2020).</p>
<p>Slawski, Bill: How Google Uses Named Entity Disambiguation for Entities with the Same Names. 2015. <a href="https://www.seobythesea.com/2015/09/disambiguate-entities-in-"><em>https://www.seobythesea.com/2015/09/disambiguate-entities-in-</em></a><a href="https://www.seobythesea.com/2015/09/disambiguate-entities-in-queries-and-pages/"><em>queries-and-pages/</em></a> (4.12.2020).</p>
<p>Steeg, Fabian, Adrian Pohl u. Pascal Christoph: lobid-gnd – Eine Schnittstelle zur Gemeinsamen Normdatei für Mensch und Maschine. Informationspraxis 5 (1). 2019. doi:<a href="https://doi.org/10.11588/ip.2019.1.52673"><em>https://doi.org/10.11588/ip.2019.1.52673</em></a>.</p>
<p>Target, Sinclair: The Rise and Rise of JSON. 2017. <a href="https://twobithistory.org/2017/09/21/the-rise-and-rise-of-json.html"><em>https://twobithistory.org/2017/09/21/the-rise-and-rise-of-json.html</em></a> (4.12.2020).</p>
<div class="footnotes">
<hr />
<ol>
<li id="fn1"><p>Details zu diesem Beispiel finden sich unter https://blog.lobid.org/2019/09/30/openrefine-examples.html#occupations-and-affiliations (4.12.2020).<a href="#fnref1">↩</a></p></li>
<li id="fn2"><p>Die Beschreibung des Protokolls verwendet die englischen Begriffe (kursiv gesetzt) aus dem Spezifikationsentwurf (Delpeuch et al. 2020).<a href="#fnref2">↩</a></p></li>
<li id="fn3"><p>Für Details und weitere Beispiele siehe https://shapeshed.com/jq-json/ (4.12.2020).<a href="#fnref3">↩</a></p></li>
<li id="fn4"><p>Details zu diesem Beispiel finden sich unter https://blog.lobid.org/2019/09/30/openrefine-examples.html#occupations-and-affiliations (4.12.2020).<a href="#fnref4">↩</a></p></li>
<li id="fn5"><p>Die vollständige Charter der Gruppe findet sich unter https://reconciliation-api.github.io/charter/ (4.12.2020)<a href="#fnref5">↩</a></p></li>
<li id="fn6"><p>https://lists.w3.org/Archives/Public/public-reconciliation/2020Jul/0000.html (4.12.2020).<a href="#fnref6">↩</a></p></li>
<li id="fn7"><p>https://github.com/reconciliation-api/specs/pull/38 (4.12.2020).<a href="#fnref7">↩</a></p></li>
<li id="fn8"><p>https://github.com/reconciliation-api (4.12.2020).<a href="#fnref8">↩</a></p></li>
<li id="fn9"><p>https://reconciliation-api.github.io/testbench/ (4.12.2020).<a href="#fnref9">↩</a></p></li>
<li id="fn10"><p>https://reconciliation-api.github.io/census/services/#how-to-add-a-service-to-the-test-bench (4.12.2020).<a href="#fnref10">↩</a></p></li>
<li id="fn11"><p>https://reconciliation-api.github.io/census/ (4.12.2020).<a href="#fnref11">↩</a></p></li>
<li id="fn12"><p>https://developers.exlibrisgroup.com/blog/how-to-install-and-use-the-alma-refine-cloud-app-2-2/ (4.12.2020).<a href="#fnref12">↩</a></p></li>
<li id="fn13"><p>https://developers.exlibrisgroup.com/blog/how-to-use-the-alma-refine-cloud-app-for-service-gnd/ (4.12.2020).<a href="#fnref13">↩</a></p></li>
</ol>
</div>
